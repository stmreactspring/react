import React, {Component} from "react";
import {Measurement} from "../core/model/Measurement";
import {AxiosInstance} from "axios";
import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from 'recharts';
import styled from "styled-components";
import {Button, Input, TextField} from "@material-ui/core";
import {DataChart, DataChartProps} from "./DataChart";
import {isTSPropertySignature} from "@babel/types";


const Comp = styled.div`
      height: 123px;
      width: 200px;
    `;

interface IState {
    host: string;
    path: string;
    xValueKey: string;
    yValueKey: string;
    yValueName: string;
    formatXasTime: boolean;
}

export class MeasTable extends Component<{}, DataChartProps> {


    private measurementsURL = '/api/getPage?page=0&size=20';


    private axios: AxiosInstance = require('axios');

    private data: Measurement[] = [];

    private interval: any;

    private charts: any = [];

    private i: number = 0;

    constructor(props: any) {
        super(props);
        this.axios.defaults.baseURL = 'http://localhost:8080';
        this.fetchData();
        this.interval = setInterval(() => this.fetchData(), 10000);
        this.onNewChart = this.onNewChart.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    fetchData() {

        this.axios.get(this.measurementsURL).then(response => {
             this.data = response.data;
             this.data.forEach( element => element.creationDate = new Date(element.creationDate).toLocaleTimeString());
             this.data = this.data.reverse();
             this.setState({});
        });
    }

    onSubmit(event: any) {
        console.log(event);
    }

    onNewChart(event: any) {
        event.preventDefault();

        const ev = event.target;
        const props: DataChartProps = {
          host: ev.host.value,
          path: ev.path.value,
          formatXasTime: true,
          interval: 10000,
          xValueKey: ev.xValueKey.value,
          yValueKey: ev.yValueKey.value,
          yValueName: ev.yValueName.value
        };

        const listItems = <div key={"popo" + this.i++} className="container"><DataChart key={"asd"} {...props} /></div>;

        this.charts.push(listItems);

        console.log('charts' + this.charts);
        this.setState({...this.state});
    }

    render() {
        console.log(this.data);
        const props = { host: 'http://localhost:8080', path: '/api/getPage?page=0&size=24',
            xValueKey: 'creationDate', yValueKey: 'temperature',
            yValueName: 'Temperatura', formatXasTime: true, interval: 10000};

        return (
            <div className="col-container">
                <form onSubmit={this.onNewChart}>
                <div className="TextBoxContainer">
                    <div className="TextBox">
                        <TextField
                            id="host"
                            label="Host"
                            defaultValue="http://localhost:8080"
                            onChange={ e => this.setState({ host: e.target.value}) }
                        />
                    </div>
                    <div className="TextBox">
                        <TextField
                            id="path"
                            label="Path"
                            defaultValue="/api/getPage?page=0&size=24"
                            onChange={ e => this.setState({ path: e.target.value}) }
                        />
                    </div>
                    <div className="TextBox">
                        <TextField
                            id="xValueKey"
                            label="xValueKey"
                            defaultValue="creationDate"
                            onChange={ e => this.setState({ xValueKey: e.target.value}) }
                        />
                    </div>
                    <div className="TextBox">
                        <TextField
                            id="yValueKey"
                            label="yValueKey"
                            defaultValue="temperature"
                            onChange={ e => this.setState({ yValueKey: e.target.value}) }
                        />
                    </div>
                    <div className="TextBox">
                        <TextField
                            id="yValueName"
                            label="yValueName"
                            defaultValue="Temp"
                            onChange={ e => this.setState({ yValueName: e.target.value}) }
                        />
                    </div>
                    <div className="TextBox">
                        <Button type="submit" variant="contained" color="primary">
                            Zatwierdź
                        </Button>
                    </div>
                </div>
                </form>


                {/* example chart*/}
                <div className="container">
                    <LineChart width={1230} height={250} data={this.data}
                               margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="creationDate"/>
                        <YAxis />
                        <Tooltip />
                        <Legend/>
                        <Line type="basis" dataKey="humidity" stroke="#8884d8" dot={false}/>
                        <Line type="basis" dataKey="temperature" stroke="#82ca9d" dot={false}/>
                    </LineChart>
                </div>

                {/* example chart*/}
                <div className="container">
                    <LineChart width={1230} height={250} data={this.data}
                               margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="creationDate"/>
                        <YAxis />
                        <Tooltip />
                        <Legend/>
                        <Line type="basis" name={'PM1'} dataKey="particulateMatter.pm1" stroke="#8884d8" dot={false}/>
                        <Line type="basis" name={'PM2,5'} dataKey="particulateMatter.pm2" stroke="#82ca9d" dot={false}/>
                        <Line type="basis" name={'PM10'} dataKey="particulateMatter.pm10" stroke="#ffca9d" dot={false}/>
                    </LineChart>
                </div>

                {/* generated chart*/}
                <span key={"generated"}>
                    {this.charts}
                </span>
            </div>
        );
    }
}