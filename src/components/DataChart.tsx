import React from "react";
import {AxiosInstance} from "axios";
import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";

export interface DataChartProps {
    host: string;
    path: string;
    xValueKey: string;
    yValueKey: string;
    yValueName: string;
    formatXasTime: boolean;
    interval: number;
}


export class DataChart extends React.Component<DataChartProps,{}> {

    private axios: AxiosInstance = require('axios');

    private data: any[] = [];

    private interval: any;


    constructor(props: any) {
        super(props);
        this.axios.defaults.baseURL = this.props.host;
        this.fetchData();
        this.interval = setInterval(() => this.fetchData(), 10000);
    }


    fetchData() {
        this.axios.get(this.props.path)
            .then( response => response.data)
            .then( data => {
                this.data = data.reverse();
                // foreach nie działa :(
                for ( let element of this.data)
                    element[this.props.xValueKey] = new Date(element[this.props.xValueKey]).toLocaleTimeString();
                this.setState({});
            });
    }

    render() {
        return (
          <div>
              { this.data.length == 0 ? (
                  <p>Nothing to show</p>
              ) : (
                  <LineChart width={1230} height={250} data={this.data}
                             margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                      <CartesianGrid strokeDasharray="3 3"/>
                      <XAxis dataKey={this.props.xValueKey}/>
                      <YAxis type="number" domain={[0, 'dataMax + 8']}/>
                      <Tooltip />
                      <Legend/>
                      <Line type="basis"  name={this.props.yValueName} dataKey={this.props.yValueKey} stroke="#ffca9d" dot={false}/>
                  </LineChart>
              )}
          </div>
        );
    }

}