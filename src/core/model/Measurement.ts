export interface ParticulateMatter {
    pm1: number;
    pm2: number;
    pm10: number;
}

export interface Measurement {
    creationDate: any;
    temperature: number;
    humidity: number;
    particulateMatter: ParticulateMatter;
}